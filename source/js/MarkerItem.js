
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

function MarkerItem (props) {

  let selectMarker = () => {
    props.selectMarker(props.uid)
  };

  let deleteMarker = event => {
    event.stopPropagation();
    props.deleteMarker(props.uid)
  };

  let changeMarkerItem = event => {
    event.stopPropagation();
    props.changeMarkerItem(props.uid, prompt('Изменить название объекта', props.title) || props.title);
  };


  return (
    <div
      className={`list__item ${props.selected?'list__item-selected':''}`}
      onClick={selectMarker}>
      <span
        className="list__item-title">{props.title}</span>
      <i onClick={changeMarkerItem} className="fa fa-pencil list__item-icon" title="Редактировать метку"/>
      <i onClick={deleteMarker} className="fa fa-times list__item-icon" title="Удалить метку"/>

    </div>);

}

export default connect(
  state => state,
  dispatch => ({
    changeMarkerItem: (uid, title) => {
      dispatch({type: 'CHANGE_MARKER', data: {uid, title}})
    },
    selectMarker: uid => {
      dispatch({type: 'SELECT_MARKER', data: {uid}})
    },
    deleteMarker: uid => {
      dispatch({type: 'DELETE_MARKER', data: {uid}});
      dispatch({type: 'DELETE_OBJECT_BY_MARKER_UID', data: {markerUid: uid}});
    }
  })
)(MarkerItem)
