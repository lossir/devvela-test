
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import MarkerItem from './MarkerItem';

function MarkerList (props) {

  let empty = <div className="block__item-empty">- Кликните по карте, чтобы добавить метку -</div>;

  let list = <div className="list">
    {props.markers.map((item, index) => {
      return <MarkerItem
        key={index}
        uid={item.uid}
        title={item.title}
        selected={item.selected}/>
    })}
  </div>;

  return <div className="block__item block__item_left">
    <div className="block__item-title">Список меток</div>
    {props.markers.length
      ? list
      : empty}

  </div>;

}

export default connect(
  state => state,
  dispatch => ({})
)(MarkerList)
