
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import ObjectItem from './ObjectItem';

const guid = () => Math.random().toString(16).substring(2);

function ObjectList (props) {

  // Получаем uid выбранного маркера
  let currentMarkerItemUid = null;
  currentMarkerItemUid = props.markers.some(item=>{
    currentMarkerItemUid = item.uid;
    return item.selected==true
  }) ? currentMarkerItemUid : null;

  // Создаём массив из объектов, принадлежащих выбранному маркеру
  let objects = props.objects.filter(
    ({markerUid, uid, title}, index) => currentMarkerItemUid == markerUid
  );

  let empty = <div className="block__item-empty">- Выберите метку, чтобы добавить объект -</div>;
  
  let list = <div className="list">
    {objects.map(({markerUid, uid, title}, index) =>
      <ObjectItem
        key={Math.random()}
        title={title}
        uid={uid}/>)}
  </div>;

  let addObjectsItem = () => {
    let title = prompt('Введите название объекта');
    if (title !== null) {
      props.addObjectsItem(currentMarkerItemUid, title, guid());
    }
  };

  return <div className="block__item block__item_right">
    <div className="block__item-title">Список объектов</div>
    {objects.length
      ? list
      : empty}
    {currentMarkerItemUid ? <div className="block__item-footer">
      <button onClick={addObjectsItem} className="button">Добавить объект</button>
    </div> : ''}

  </div>;

}

export default connect(
  state => state,
  dispatch => ({
    addObjectsItem: (markerUid, title, uid) => {
      dispatch({
        type: 'ADD_OBJECT',
        data: { markerUid, title, uid }
      })
    }
  })
)(ObjectList)
