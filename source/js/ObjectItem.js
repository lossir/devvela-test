
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

function ObjectItem (props) {


  let selectObjectItem = () => {
    props.changeObjectItem(props.uid, prompt('Изменить название объекта', props.title) || props.title);
  };

  let deleteObjectItem = event => {
    event.stopPropagation();
    props.deleteObjectItem(props.uid)
  };

  return (
    <div
      className={`list__item ${props.selected?'list__item-selected':''}`}>
      <span
        className="list__item-title">{props.title}</span>
      <i onClick={selectObjectItem} className="fa fa-pencil list__item-icon" title="Редактировать объект" />
      <i onClick={deleteObjectItem} className="fa fa-times list__item-icon" title="Удалить объект" />
    </div>);

}

export default connect(
  state => state,
  dispatch => ({
    changeObjectItem: (uid, title) => {
      dispatch({type: 'CHANGE_OBJECT', data: {uid, title}})
    },
    deleteObjectItem: ( uid) => {
      dispatch({
        type: 'DELETE_OBJECT',
        data: { uid }
      })
    }
  })
)(ObjectItem)
