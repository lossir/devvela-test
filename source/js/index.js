import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import { composeWithDevTools } from 'redux-devtools-extension';

import reducer from './reducers/index';

import MarkerList from './MarkerList';
import ObjectList from './ObjectList';

let storage = localStorage ? JSON.parse(localStorage.getItem('devvela') || '{}') : {};

const store = createStore(reducer, storage, composeWithDevTools());


var GoogleMap;
window.allMarkers = {};

store.subscribe(() => {
  if (localStorage) {
    localStorage.setItem('devvela', JSON.stringify(store.getState()));
  }
});

const guid = () => Math.random().toString(16).substring(2);

function addMarkerMap (markerData) {

  let marker = new google.maps.Marker({
    position: new google.maps.LatLng( ...markerData.latlng ),
    title: markerData.title,
    map: GoogleMap,
    uid: markerData.uid,
    animation: google.maps.Animation.DROP
  });


  marker.delete = function () {
    this.setMap(null);
  };

  marker.addListener('click', function (e) {

    store.dispatch({
      type: 'SELECT_MARKER',
      data: {
        uid: this.uid
      }
    });

  });

  window.allMarkers[markerData.uid] = marker;

  return marker;

}

function createMarker (markerData) {

  addMarkerMap(markerData);

  store.dispatch({
    type: 'ADD_MARKER',
    data: markerData
  });
  
  store.dispatch({
    type: 'SELECT_MARKER',
    data: {
      uid: markerData.uid
    }
  });

}

window.initMap = () => {

  GoogleMap = new google.maps.Map(document.getElementById('map'), {
    zoom: 14,
    center: {lat: 56.838315917701934, lng: 60.603461265563965}
  });

  store.getState().markers.map(addMarkerMap);

  GoogleMap.addListener('click', function(data) {
    // addMarker(data.latLng.lat(), data.latLng.lng())
    let title = prompt('Введите название метки');
    if (title !== null) {
      createMarker({
        uid: guid(),
        title: title,
        latlng: [data.latLng.lat(), data.latLng.lng()]
      })
    }
  });

  window.GoogleMap = GoogleMap;

};


ReactDOM.render(
  <Provider store={store}>
    <MarkerList />
  </Provider>,
  document.getElementById('markers')
);

ReactDOM.render(
  <Provider store={store}>
    <ObjectList />
  </Provider>,
  document.getElementById('objects')
);
