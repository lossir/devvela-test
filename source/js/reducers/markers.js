const initialState = [];

const getMarkerByUid = (objects, uid) => {
  let index;
  objects.some((item, i) => {
    index = i;
    return item.uid == uid;
  });
  return objects[index] || null;
};

export default function (state = initialState, {type, data}) {

  if (type == 'ADD_MARKER') {

    return [{
      latlng: data.latlng,
      uid: data.uid,
      title: data.title
    }, ...state];

  }
  if (type == 'CHANGE_MARKER') {
    let markerItem = getMarkerByUid(state, data.uid);
    state[state.indexOf(markerItem)].title = data.title;
    
    return [...state];
    
  }
  if (type == 'DELETE_MARKER') {
    window.allMarkers[data.uid].delete();

    return state.filter(item => item.uid !== data.uid);
    
  }
  if (type == 'SELECT_MARKER') {

    return state.map((item, index) => {
      if (data.uid == item.uid) {
        GoogleMap.panTo({lat: item.latlng[0], lng: item.latlng[1]});
      }
      item.selected = data.uid == item.uid;
      return item;
    })

  }

  return state;
}
