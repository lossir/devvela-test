import { combineReducers } from 'redux';

import markers from '../reducers/markers';
import objects from '../reducers/objects';

export default combineReducers({
  markers,
  objects
})
