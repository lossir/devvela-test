const initialState = [];

const getObjectByUid = (objects, uid) => {
  let index;
  objects.some((item, i) => {
    index = i;
    return item.uid == uid;
  });
  return objects[index] || null;
};

export default function (state = initialState, {type, data}) {

  if (type == 'ADD_OBJECT') {
    return [{
      markerUid: data.markerUid,
      uid: data.uid,
      title: data.title
    }, ...state];
  }
  if (type == 'CHANGE_OBJECT') {
    let objectItem = getObjectByUid(state, data.uid);
    state[state.indexOf(objectItem)].title = data.title;
    return [...state];
  }
  if (type == 'DELETE_OBJECT') {

    return state.filter(item => item.uid !== data.uid);
  }
  if (type == 'DELETE_OBJECT_BY_MARKER_UID') {

    return state.filter(item => item.markerUid !== data.markerUid);
  }

  return state;
}
