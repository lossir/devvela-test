'use strict';

const fs = require('fs');
const gulp = require('gulp');
const less = require('gulp-less');
const base64 = require('gulp-base64');
const debug = require('gulp-debug');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');

let
  source = './source',
  output = './output';


// ok
gulp.task('less', () => {

  return gulp
    .src([
      `${source}/less/main.less`
    ])
    .pipe(sourcemaps.init())
    .pipe(debug())
    .pipe(less({
      compress: true
    }))
    .pipe(base64({
      extensions  : ['svg', 'png', 'jpg'],
      maxImageSize: 1024 * 1024
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(`${output}/assets/css`));

});



gulp.task('watch', () => {

  gulp.watch(`${source}/less/`, gulp.series('less'));

});
