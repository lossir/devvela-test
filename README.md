# devvela-test #

> Тестовое задание на фронденд разработчика.

### Работа с проектом #

```sh
# установить и запустить node-static
$ npm run start
serving "output" at http://127.0.0.1:8080

# установить все зависимости и собрать проект
$ npm run build

# запустить gulp и webpack в watch режиме
$ npm run dev

# если пакет node-static уже установлен глобально
$ static output
```

***
**`node-static`** необходим, чтобы `localStorage` корректно работал в IE/Edge
***
